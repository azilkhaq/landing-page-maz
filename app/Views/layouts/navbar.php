<div class="header-style-01">
    <!-- support bar area end -->
    <nav class="navbar navbar-area navbar-expand-lg nav-style-02">
        <div class="container nav-container social-nav">
            <div class="responsive-mobile-menu">
                <div class="logo-wrapper">
                    <!-- <a href="index.html" class="logo">
                        <img src="<?= base_url('public/assets/img/logo-02.png') ?>" alt="">
                    </a> -->
                    <h2 style="color: white;">MAZ</h2>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bizcoxx_main_menu" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bizcoxx_main_menu">
                <ul class="navbar-nav">
                    <li><a href="#home">Home</a></li>
                    <li><a href="#executive">Executive Summary</a></li>
                    <li><a href="#technical">Technical Solution</a></li>
                    <li><a href="#features">Features</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- navbar area end -->
</div>