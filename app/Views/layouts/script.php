<!-- jquery -->
<script src="<?= base_url('public/assets/js/jquery-2.2.4.min.js') ?>"></script>
<!-- bootstrap -->
<script src="<?= base_url('public/assets/js/bootstrap.min.js') ?>"></script>
<!-- magnific popup -->
<script src="<?= base_url('public/assets/js/jquery.magnific-popup.js') ?>"></script>
<!-- wow -->
<script src="<?= base_url('public/assets/js/wow.min.js') ?>"></script>
<!-- owl carousel -->
<script src="<?= base_url('public/assets/js/owl.carousel.min.js') ?>"></script>
<!-- waypoint -->
<script src="<?= base_url('public/assets/js/waypoints.min.js') ?>"></script>
<!-- counterup -->
<script src="<?= base_url('public/assets/js/jquery.counterup.min.js') ?>"></script>
<!-- Water effect -->
<script src="<?= base_url('public/assets/js/jquery.ripples-min.js') ?>"></script>
<!-- VanillaTilt effect -->
<script src="<?= base_url('public/assets/js/tilt.jquery.js') ?>"></script>
<!-- imageloaded -->
<script src="<?= base_url('public/assets/js/imagesloaded.pkgd.min.js') ?>"></script>
<!-- isotope -->
<script src="<?= base_url('public/assets/js/isotope.pkgd.min.js') ?>"></script>
<!-- parallax js -->
<script src="<?= base_url('public/assets/js/parallax.js') ?>"></script>
<!-- main js -->
<script src="<?= base_url('public/assets/js/main.js') ?>"></script>