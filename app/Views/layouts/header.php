<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>MAZ - Nusantara Cakti</title>
    <!-- favicon -->
    <link rel=icon href=favicon.ico sizes="20x20" type="image/png">
    <!-- animate -->
    <link rel="stylesheet" href="<?= base_url('public/assets/css/animate.css') ?>">
    <!-- bootstrap -->
    <link rel="stylesheet" href="<?= base_url('public/assets/css/bootstrap.min.css') ?>">
    <!-- magnific popup -->
    <link rel="stylesheet" href="<?= base_url('public/assets/css/magnific-popup.css') ?>">
    <!-- owl carousel -->
    <link rel="stylesheet" href="<?= base_url('public/assets/css/owl.carousel.min.css') ?>">
    <!-- fontawesome -->
    <link rel="stylesheet" href="<?= base_url('public/assets/css/font-awesome.min.css') ?>">
    <!-- flaticon -->
    <link rel="stylesheet" href="<?= base_url('public/assets/css/flaticon.css') ?>">
    <!-- Hover CSS -->
    <link rel="stylesheet" href="<?= base_url('public/assets/css/hover-min.css') ?>">
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="<?= base_url('public/assets/css/style.css') ?>">
    <!-- responsive Stylesheet -->
    <link rel="stylesheet" href="<?= base_url('public/assets/css/responsive.css') ?>">

    <style>
        .enterprice-ready-area .section-title .title-2 {
            font-size: 35px;
            line-height: 52px;
            font-weight: 600;
            margin-bottom: 25px;
        }

        .title-3 {
            font-size: 25px;
            color: white;
        }

        .single-2{
            height: 178px !important;
        }

        .item-2 {
            height: 306px !important;
        }

        .item-3 {
            height: 373px !important;
        }
    </style>
</head>