 <!-- footer area start -->
 <footer class="footer-area">
     <div class="copyright-area style-03" style="background-color: #343da7;">
         <div class="container">
             <div class="row">
                 <div class="col-lg-12">
                     <div class="copyright-area-inner" style="color: white;">
                         © Copyrights 2020 Maz Nusantara Cakti All rights reserved.
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </footer>
 <!-- footer area end -->